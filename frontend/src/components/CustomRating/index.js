import React from 'react'
import { Rating } from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    '& > * + *': {
      marginTop: theme.spacing(1)
    }
  }
}))

const propTypes = {
  defaultValue: PropTypes.number
}

const defaultProps = {
  defaultValue: 0
}

function CustomRating (props) {
  const classes = useStyles()
  return (
        <div className={classes.root}>
            <Rating name="half-rating-read" defaultValue={props.defaultValue} precision={0.5} readOnly />
        </div>
  )
}

CustomRating.propTypes = propTypes
CustomRating.defaultProps = defaultProps

export default CustomRating
