import conn from '../../../services/conn'

async function getBook (bookId) {
  return await conn.get(`book/${bookId}`).catch(() => { return { error: 'Falhar ao tentar Listar o Livro' } })
}

async function updateOrCreateBook (book) {
  return book.id > 0
    ? await conn.put('books', book).catch(() => { return { error: 'Falhar ao tentar Atualizar o Livro' } })
    : await conn.post('books', book).catch(() => { return { error: 'Falhar ao tentar Cadastrar o Livro' } })
}

async function deleteBook (bookId) {

}

export default {
  getBook,
  updateOrCreateBook,
  deleteBook
}
