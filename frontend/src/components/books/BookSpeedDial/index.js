import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import * as Icons from '@material-ui/icons/'
import PropTypes from 'prop-types'
import controller from './controller.js'
import CustomSpeedDial from '../../CustomSpeedDial'
import BookDialog from '../BookDialog/index.js'

const useStyles = makeStyles((theme) => ({
  speedDial: {
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  }
}))

const propTypes = {
  speedDialOptions: PropTypes.object.isRequired,
  selectedRowId: PropTypes.number
}

function BookSpeedDial (props) {
  const classes = useStyles()
  const [openBookModal, setOpenBookModal] = useState(false)
  const [book, setBook] = useState({})

  const handleBookModalOpen = () => {
    setOpenBookModal(true)
  }

  const handleBookModalClose = () => {
    setOpenBookModal(false)
  }

  const getActions = (selectedRowId) => {
    const actions = []
    if (selectedRowId) {
      actions.push({
        icon: <Icons.Delete />,
        name: 'Remover o Livro',
        handleClick: () => {

        }
      })
      actions.push({
        icon: <Icons.Edit />,
        name: 'Editar Livro',
        handleClick: () => {
          controller.getBook(props.selectedRowId).then(book => {
            if (book.data.error) {
              alert(book.data.error)
            } else {
              console.log(book.data)
              setBook(book.data)
              setOpenBookModal(true)
            }
          })
        }
      })
    }
    actions.push({
      icon: <Icons.Add />,
      name: 'Adicionar novo Livro',
      handleClick: async () => {
        setBook({})
        setOpenBookModal(true)
        props.speedDialOptions.reloadBooks()
      }
    })

    return actions
  }

  const handleDialogOk = async (newBook) => {
    await controller.updateOrCreateBook(newBook).then((data) => {
      if (data && data.error) {
        alert(data.error)
      } else {
        setOpenBookModal(false)
        console.log(data)
      }
    })
  }

  return (
    <>
      <BookDialog
        book={book}
        handleDialogOk={handleDialogOk}
        handleOpen={handleBookModalOpen}
        handleClose={handleBookModalClose}
        open={openBookModal}
      />
      <CustomSpeedDial
      className={classes.speedDial}
      icon={<Icons.ExpandLess/>}
      hidden={props.speedDialOptions.hidden}
      actions={getActions(props.selectedRowId)}
      openIcon={<Icons.ExpandMore/>}
      />
    </>
  )
}

BookSpeedDial.propTypes = propTypes

export default BookSpeedDial
