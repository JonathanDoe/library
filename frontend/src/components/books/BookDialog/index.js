import React, { useEffect, useState } from 'react'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core'
import { KeyboardDatePicker } from '@material-ui/pickers'
import useMediaQuery from '@material-ui/core/useMediaQuery'

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}))

const propTypes = {
  book: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    resume: PropTypes.string,
    author: PropTypes.string,
    dateLaunch: PropTypes.any
  }),
  open: PropTypes.bool.isRequired,
  handleOpen: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleDialogOk: PropTypes.func.isRequired
}

function BookDialog (props) {
  const classes = useStyles()
  const [bookId, setBookId] = useState(0)
  const [title, setTitle] = useState('')
  const [resume, setResume] = useState('')
  const [author, setAuthor] = useState('')
  const [dateLaunch, setDateLaunch] = useState(Date())

  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

  useEffect(() => {
    if (props.book.id) {
      (() => {
        setBookId(props.book.id)
        setTitle(props.book.title)
        setResume(props.book.resume)
        setAuthor(props.book.author)
        setDateLaunch(props.book.dateLaunch)
      })()
    }
  }, [props.book])

  return (
    <Dialog open={props.open} onClose={() => {
      props.handleClose()
      setBookId('')
      setTitle('')
      setResume('')
      setAuthor('')
      setDateLaunch(new Date().toString())
    }} aria-labelledby="form-dialog-title" fullScreen={fullScreen}>
      <DialogTitle id="form-dialog-title">{ bookId > 0 ? 'Edição do Livro: ' + title : 'Novo Cadastro'}</DialogTitle>
      <DialogContent>
        <TextField
          label="Titulo"
          id="title"
          margin="normal"
          value={title}
          onChange={(e) => { setTitle(e.target.value) }}
          defaultValue=""
          fullWidth
        />
        <TextField
          label="Sinopse"
          id="resume"
          margin="normal"
          value={resume}
          onChange={(e) => setResume(e.target.value)}
          defaultValue=""
          fullWidth
        />
        <TextField
          label="Autor"
          id="author"
          margin="normal"
          value={author}
          onChange={(e) => setAuthor(e.target.value)}
          defaultValue=""
          fullWidth
        />
         <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="dd/MM/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Data de Lançamento"
          value={dateLaunch}
          onChange={(e) => setDateLaunch(e)}
          defaultValue=""
          KeyboardButtonProps={{
            'aria-label': 'change date'
          }}
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={() => {
          props.handleClose()
          setBookId('')
          setTitle('')
          setResume('')
          setAuthor('')
          setDateLaunch(new Date().toString())
        }} color="secondary" className={classes.btnCancel}>
          Cancelar
        </Button>
        <Button onClick={async () => {
          await props.handleDialogOk({
            id: bookId,
            title,
            resume,
            author,
            dateLaunch
          })
          setBookId('')
          setTitle('')
          setResume('')
          setAuthor('')
          setDateLaunch(new Date().toString())
        }} color="primary"className={ classes.btnSave}>
          Salvar
        </Button>
      </DialogActions>
    </Dialog>
  )
}

BookDialog.propTypes = propTypes

export default BookDialog
