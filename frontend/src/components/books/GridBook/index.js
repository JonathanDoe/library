import dateFormat from 'dateformat'
import React from 'react'
import PropTypes from 'prop-types'
import PagedCustomGrid from '../../Table/PagedCustomGrid'
import InfoToolTip from '../../Tooltip/InfoToolTip'
import CustomRating from '../../CustomRating'

const propTypes = {
  gridOptions: PropTypes.object
}

const columns = [
  { field: 'id', hide: true },
  { field: 'title', headerName: 'Titulo', width: 220 },
  { field: 'resume', headerName: 'Resumo', width: 275 },
  { field: 'author', headerName: 'Autor', width: 180 },
  {
    field: 'dateLaunch',
    headerName: 'Dt. Lançamento',
    width: 175,
    valueFormatter: (params) => dateFormat(new Date(params.value), 'dd/mm/yyyy'),
    align: 'right'

  },
  {
    field: 'rent',
    headerName: 'Status',
    width: 160,
    renderCell: (params) => {
      const isRented = params.row.rent && params.row.rent.status === 1
      if (isRented) {
        return (<>
          <span>Alugado</span> &nbsp; <InfoToolTip title={`Data de Entrega: ${dateFormat(new Date(params.row.rent.endRent), 'dd/mm/yyyy')}`} />
        </>)
      } else {
        return (<span>Disponível</span>)
      }
    }
  },
  {
    field: 'averageRate',
    headerName: 'Avaliação Média',
    width: 180,
    renderCell: (params) => {
      if (params.row.averageRate != null) {
        return (<CustomRating defaultValue={params.row.averageRate} />)
      } else { return null }
    }
  }, {
    field: ''
  }
]

const createRows = rows => {
  if (!rows) {
    return []
  }
  return rows.map(row => row)
}

function GridBook (props) {
  return (<PagedCustomGrid
    columns={columns}
    createRows={createRows}
    gridOptions={props.gridOptions}
  />)
}

GridBook.propTypes = propTypes

export default GridBook
