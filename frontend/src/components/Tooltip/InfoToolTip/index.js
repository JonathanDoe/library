import React from 'react'
import { Info } from '@material-ui/icons/'
import PropTypes from 'prop-types'
import BootstrapTooltip from '../BootstrapTooltip'

const propTypes = {
  title: PropTypes.string.isRequired
}

function InfoToolTip (props) {
  return (
        <BootstrapTooltip title={props.title} arrow interactive>
            <Info color="primary" />
        </BootstrapTooltip>
  )
}

InfoToolTip.propTypes = propTypes

export default InfoToolTip
