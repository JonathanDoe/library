import React from 'react'
import { Tooltip } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: theme.palette.common.white,
    border: 2,
    borderColor: theme.palette.common.black
  },
  tooltip: {
    backgroundColor: theme.palette.common.white,
    border: 0,
    borderRadius: 2,
    borderColor: theme.palette.common.black,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: theme.palette.common.black
  }
}))

export default function BootstrapTooltip (props) {
  const classes = useStylesBootstrap()

  return (<>
    <Tooltip arrow classes={classes} {...props} />
    </>)
}
