import React, { useState } from 'react'
import { SpeedDial, SpeedDialIcon, SpeedDialAction } from '@material-ui/lab'
import PropTypes from 'prop-types'

const propTypes = {
  icon: PropTypes.object.isRequired,
  hidden: PropTypes.bool.isRequired,
  actions: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    icon: PropTypes.object.isRequired,
    handleClick: PropTypes.func.isRequired
  })).isRequired,
  openIcon: PropTypes.object.isRequired
}

function CustomSpeedDial (props) {
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }
  const handleOpen = () => {
    setOpen(true)
  }

  return (
        <SpeedDial
            ariaLabel="Ações"
            hidden={props.hidden}
            icon={<SpeedDialIcon icon={props.icon} openIcon={props.openIcon} />}
            onClose={handleClose}
            onOpen={handleOpen}
            open={open}
            direction={'up'}
        >
            {
                props.actions.map((action) => (
                    <SpeedDialAction
                        key={action.name}
                        icon={action.icon}
                        tooltipTitle={action.name}
                        onClick={action.handleClick}
                    />
                ))
            }
        </SpeedDial>
  )
}

CustomSpeedDial.propTypes = propTypes

export default CustomSpeedDial
