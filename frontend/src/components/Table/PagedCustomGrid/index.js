import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { DataGrid } from '@material-ui/data-grid'
import PropTypes from 'prop-types'
// import BookSpeedDial from '../../books/BookSpeedDial'

const useStyles = makeStyles({
  root: {
    height: 400, width: '100%'
  }
})

const propTypes = {
  columns: PropTypes.array.isRequired,
  createRows: PropTypes.func.isRequired,
  gridOptions: PropTypes.shape({
    page: PropTypes.number.isRequired,
    rows: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    handlePageChange: PropTypes.func.isRequired,
    handleRowsPerPage: PropTypes.func.isRequired,
    handleSelectionModelChange: PropTypes.func.isRequired
  })
}

function PagedCustomGrid (props) {
  const classes = useStyles()
  const rows = props.createRows(props.gridOptions.rows)

  return (
        <div className={classes.root}>
            <DataGrid
                disableColumnMenu
                rows={rows || []}
                columns={props.columns || []}
                pagination
                pageSize={props.gridOptions.rowsPerPage}
                rowCount={100}
                paginationMode="server"
                onPageChange={props.gridOptions.handlePageChange}
                loading={props.gridOptions.loading}
                onSelectionModelChange={props.gridOptions.handleSelectionModelChange}
            >

      </DataGrid>

    </div >
  )
}

PagedCustomGrid.propTypes = propTypes

export default PagedCustomGrid
