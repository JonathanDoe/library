
import React from 'react'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { AppBar, Toolbar, Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    title: {
      flexGrow: 1
    },
    content: {
      marginTop: theme.spacing(10)
    }
  })
)

function Header () {
  const classes = useStyles()
  return (<AppBar position="static" maxwidth='lg'>
        <Toolbar>
            <Typography variant="h6" className={classes.title}>
                Lista de Livros
             </Typography>
        </Toolbar>
    </AppBar>)
}

export default Header
