import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import ListBook from './pages/admin/ListBook'

export default function Routes () {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={ListBook} />
      </Switch>
    </BrowserRouter>
  )
}
