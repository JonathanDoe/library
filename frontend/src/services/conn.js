import axios from 'axios'

const urlApi = 'https://intense-anchorage-49530.herokuapp.com/'

export default axios.create({
  baseURL: urlApi
})
