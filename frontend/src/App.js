import './global.css'
import React from 'react'
import Routes from './routes'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import { Container } from '@material-ui/core'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'

import Header from './components/Header'
const useStyles = makeStyles((theme) =>
  createStyles({
    content: {
      marginTop: theme.spacing(10)
    }
  })
)

function App () {
  const classes = useStyles()
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Header />
      <Container className={classes.content}>
        <Routes />
      </Container>
    </MuiPickersUtilsProvider>
  )
}

export default App
