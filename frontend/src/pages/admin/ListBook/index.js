import { AppBar, Toolbar, IconButton } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import * as Icons from '@material-ui/icons'
import conn from '../../../services/conn'
import GridBook from '../../../components/books/GridBook'
import BookSpeedDial from '../../../components/books/BookSpeedDial'

async function getAllBooks (limit, page) {
  const result = await conn.get(`books?limit=${limit}&page=${page}&orderby=-id`)
  return result.data
}

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    toolBar: {
      justifyContent: 'flex-end'
    },
    searchButton: {
      marginRight: theme.spacing(0)
    },
    title: {
      flexGrow: 1
    },
    dial: {
      position: 'absolute',
      bottom: '0',
      right: '0',
      margin: '20px'
    }
  })
)

export default function ListBook () {
  const classes = useStyles()

  const [books, setBooks] = useState([])
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [loading, setLoading] = useState(false)

  const [selectedRowId, setSelectedRowId] = useState(null)

  const loadBooks = () => {
    let active = true;

    (async () => {
      setLoading(true)
      const allBooks = await getAllBooks(rowsPerPage, page)
      if (!active) {
        return
      }
      setBooks(allBooks.books)
      setLoading(false)
    })()

    return () => {
      active = false
    }
  }

  useEffect(loadBooks, [page, rowsPerPage, books])

  const handleOnClick = () => {
    loadBooks()
  }

  const handlePageChange = (newPage) => {
    setPage(newPage.page)
  }

  const handleRowsPerPage = (e) => {
    setRowsPerPage(e.target.value)
  }

  const handleSelectionModelChange = (row) => {
    const isValid = row.selectionModel[0] || row.selectionModel[0] === '0'
    setSelectedRowId(isValid ? row.selectionModel[0] : null)
  }

  const gridOptions = {
    page,
    rows: books,
    loading,
    rowsPerPage,
    handlePageChange,
    handleRowsPerPage,
    handleSelectionModelChange
  }
  const speedDialOptions = {
    hidden: false,
    reloadBooks: loadBooks
  }

  return (<>
    <div className={classes.root}>
      <AppBar position="static">
          <Toolbar className={classes.toolBar}>
              <IconButton edge="end" className={classes.searchButton} color="inherit" aria-label="menu">
                  <Icons.Search onClick={handleOnClick} />
              </IconButton>
          </Toolbar>
      </AppBar>
    </div>
    <GridBook gridOptions={gridOptions} />
    <div className={classes.dial}>
      <BookSpeedDial selectedRowId={parseInt(selectedRowId)} speedDialOptions={speedDialOptions} />
    </div>
  </>)
}
