import sequelize from "sequelize";
import conn from "../../config/conn.js"
import { user as User, book as Book, bookRent as BookRent, bookReview as BookReview } from "../model/models.js"

function getWhere(query, like) {
    const { title, resume, author } = query
    const where = {}
    if (title) {
        where.title = like ? {
            [sequelize.Op.like]: `%${title}%`
        } : title
    }
    if (resume) {
        where.resume = like ? {
            [sequelize.Op.like]: `%${resume}%`
        } : resume
    }
    if (author) {
        where.author = like ? {
            [sequelize.Op.like]: `%${author}%`
        } : author
    }

    return where
}

function getOrder(orderby) {
    let order = null
    if (orderby && (orderby.match(/(\+|\-)\w+/) || orderby.indexOf(' ') === -1)) {
        const asc = orderby.indexOf('-') === -1;
        orderby = orderby.replace(asc ? '+' : '-', '')
        order = [
            [orderby, asc ? 'ASC' : 'DESC']
        ]
    }

    return order
}

function setCustomOptions() {
    const include = {
        model: BookRent,
        required: false,
        attributes: ['id', 'startRent', 'endRent', 'status'],
        where: {
            status: 1
        },
        include: {
            model: User,
            attributes: ['id', 'firstName', 'lastName'],
        }
    }

    const attrAverage = {
        include: [
            [sequelize.literal(`(
                    SELECT AVG("reviews1"."rateValue")
                    FROM reviews AS reviews1
                    WHERE "reviews1"."bookId" = books.id
                )`),
                'averageRate']
        ]
    }

    return {
        include,
        attrAverage,
    }
}

export default {
    async get(req, resp) {
        const { id } = req.params
        const { page, limit, like, orderby } = req.query
        const { include, attrAverage } = setCustomOptions()

        try {
            const books = id
                ? await Book.findByPk(parseInt(id), {
                    include,
                    attributes: attrAverage,
                })
                : await Book.findAndCountAll({
                    offset: (page * limit) || 0,
                    limit: limit || 10,
                    order: getOrder(orderby),
                    where: getWhere(req.query, like),
                    include,
                    attributes: attrAverage,
                })

            if (books) {
                if (id) {
                    resp.json(books)
                } else {
                resp.json({
                    books: books.rows,
                    limit,
                    page: (page || 0),
                    total: books.count,
                })
                }
            } else {
                resp.json({})
            }
        } catch (error) {
            resp.json({ error: error.message })
        }
    },
    async add(req, resp) {
        const { title, resume, author } = req.body;

        Book.create({ title, resume, author }).then(newBook => {
            resp.json({ id: newBook.id })
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async update(req, resp) {
        const { id, title, resume, author } = req.body;
        Book.findByPk(id).then(book => {
            title && title != book.title && (book.title = title)
            resume && resume != book.resume && (book.resume = resume)
            author && author != book.author && (book.author = author)

            book.save().then(updatedBook => {
                resp.json({ id: updatedBook.id })
            }).catch(error => {
                resp.json({ error: error.message })
            })
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async delete(req, resp) {
        const { id } = req.params
        Book.findByPk(id).then(book => {
            book.destroy().then(() => {
                resp.status(200).json({})
            }).catch(error => {
                resp.json({ error: error.message })
            })
        }).catch(error => {
            resp.json({ error: error.message })
        })
    }
}
