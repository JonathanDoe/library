import sequelize from 'sequelize'
import { bookRent as BookRent } from '../model/models.js'
import dateformat from "dateformat";

export default {
    async getRent(req, resp) {
        const { bookId } = req.params
        BookRent.findOne({
            where: {
                bookId,
                status: 1,
            }
        }).then(rent => {
            resp.json(rent)
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async rent(req, resp) {
        const { bookId } = req.params
        const { userId, startRent, endRent } = req.body

        BookRent.findOne({
            where: {
                bookId,
                status: 1,
                endRent: {
                    [sequelize.Op.between]: [startRent, endRent]
                }
            }
        }).then(bookRent => {
            if (!bookRent) {
                BookRent.create({
                    userId,
                    bookId,
                    startRent,
                    endRent,
                    status: 1
                }).then(newRent => {
                    resp.json({ id: newRent.id })
                }).catch(error => {
                    resp.json({ error: error.message })
                })
            } else {
                if (bookRent.userId === parseInt(userId)) {
                    resp.json({ error: 'Este livro já está sendo alugado pelo mesmo usuário. Data de entrega: ' + dateformat(bookRent.endRent, 'dd/mm/yyyy') })
                } else {
                    resp.json({
                        error: 'Este livro já foi alugado. Data de entrega ' + dateformat(bookRent.endRent, 'dd/mm/yyyy')
                    })
                }
            }
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async return(req, resp) {
        const { bookId } = req.params
        const { userId } = req.body
        const resBody = {}
        BookRent.findOne({
            where: {
                bookId,
                status: 1
            }
        }).then(bookRent => {
            if (!bookRent) {
                resBody.error = 'Livro não encontrado'
                return
            }

            if (bookRent.status === 0) {
                resBody.error = 'Livro não está alugado'
                return
            }

            if (bookRent.userId !== parseInt(userId)) {
                resBody.error = 'Este livro não foi alugado pelo usuário informado'
                return
            }

            bookRent.status = 0
            bookRent.save()
            resBody.id = bookRent.id
        }).catch(error => {
            resBody.error = error.message
        }).then(() => {
            resp.json(resBody)
        })
    }
}
