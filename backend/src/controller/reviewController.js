import conn from '../../config/conn.js'
import { bookRent as Rent, bookReview as Review } from '../model/models.js'
import commons from '../utils.js/commons.js'

function validateFields(obj) {
    const resBody = {
        message: '',
        isValid: true,
    }

    if (obj.hasOwnProperty('userId') && !obj.userId) {
        resBody.message += 'Usuário não informado'
        resBody.isValid = false
    }
    if (obj.hasOwnProperty('comment') && !obj.comment) {
        resBody.message += 'Necessário informar o comentário'
        resBody.isValid = false
    }
    if (obj.hasOwnProperty('rateValue') && (!obj.rateValue || isNaN(obj.rateValue))) {
        resBody.message += 'Necessário informar quantidade de estrelas'
        resBody.isValid = false
    }

    return resBody
}

async function reviewExist(bookId, userId) {
    return await Rent.count({
        where: {
            bookId,
            userId,
        }
    })
}

async function getAvg(bookId) {
    return await Review.findOne({
        where: {
            bookId
        },
        attributes: [[conn.fn('AVG', conn.col('rateValue')), 'averageRate']],
    }).then(reviewAvg => {
        return reviewAvg
    }).catch(error => {
        return { error: error.message }
    })
}

export default {
    async getReviews(req, resp) {
        const { bookId } = req.params

        Review.findAll({
            where: {
                bookId
            },
            order: [['updatedAt', 'DESC']]
        }).then(reviews => {
            getAvg(bookId).then(avg => {
                resp.json({
                    averageRate: parseFloat(avg.get('averageRate')).toFixed(3),
                    reviews
                })
            }).catch(error => {
                resp.json({ error: error.message })
            })
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async comment(req, resp) {
        const { bookId } = req.params
        const { userId, commentary, rateValue } = req.body

        const valid = validateFields({
            userId, commentary, rateValue
        })

        if (!valid.isValid) {
            resp.json({ error: valid.message })
            return
        }

        if (await reviewExist(bookId, userId) === 0) {
            resp.json({ error: 'O livro só pode ser avaliado apos a leitura' })
            return
        }

        Review.create({
            bookId,
            userId,
            commentary,
            rateValue,
        }).then(newReview => {
            resp.json({ id: newReview.id })
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async edit(req, resp) {
        const { bookId } = req.params
        const { userId, commentary, rateValue } = req.body

        const valid = validateFields({
            userId, commentary, rateValue
        })

        if (!valid.isValid) {
            resp.json({ error: valid.message })
            return
        }

        if (await reviewExist(bookId, userId) === 0) {
            resp.json({ error: 'Você ainda não avaliou este livro' })
            return
        }

        Review.findOne({
            where: {
                bookId,
                userId,
            }
        }).then(oldReview => {
            if (oldReview) {
                commons.replaceValues(oldReview, {
                    commentary,
                    rateValue,
                })

                oldReview.save().then(updatedReview => {
                    resp.json({ id: updatedReview.id })
                }).catch(error => {
                    resp.json({ error: error.message })
                })
            } else {
                resp.json({ error: 'Avaliação não encontrada' })
            }
        }).catch(error => {
            resp.json({ error: error.message })
        })
    }
}
