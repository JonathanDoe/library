import { user as User } from "../model/models.js"
import bcrypt from "bcrypt";
import commons from "../utils.js/commons.js";

function getWhere(query, like) {
    const { firstName, lastName, email, phone, accessType } = query
    const where = {}
    if (firstName) {
        where.firstName = like ? {
            [sequelize.Op.like]: `%${firstName}%`
        } : firstName
    }
    if (lastName) {
        where.lastName = like ? {
            [sequelize.Op.like]: `%${lastName}%`
        } : lastName
    }
    if (email) {
        where.email = like ? {
            [sequelize.Op.like]: `%${email}%`
        } : email
    }
    if (phone) {
        where.phone = like ? {
            [sequelize.Op.like]: `%${phone}%`
        } : phone
    }
    if (accessType) {
        where.accessType = accessType
    }

    return where
}

export default {
    async get(req, resp) {
        const { id } = req.params
        const { offset, limit, like } = req.query
        const where = getWhere(req.query, like)
        const options = {
            offset: offset || 0,
            limit: limit || 10,
            where: where,
        }

        const users = id ? await User.findByPk(parseInt(id)) : await User.findAll(options)

        resp.json(users || {})
    },
    async add(req, resp) {
        const { firstName, lastName, email, phone, accessType, password } = req.body

        User.create({
            firstName,
            lastName,
            email,
            phone,
            accessType,
            password: bcrypt.hashSync(password, 10)
        }).then(newUser => {
            resp.json({ id: newUser.id })
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async update(req, resp) {
        const { id, firstName, lastName, email, phone, accessType, password } = req.body


        User.findByPk(id).then(oldUser => {
            if (!oldUser) {
                resp.json({})
            } else {
                commons.replaceValues({
                    firstName: oldUser.firstName,
                    lastName: oldUser.lastName,
                    email: oldUser.email,
                    phone: oldUser.phone,
                    accessType: oldUser.accessType,
                    password: oldUser.password
                }, {
                    firstName,
                    lastName,
                    email,
                    phone,
                    accessType,
                    password: bcrypt.hashSync(password, 10)
                })

                oldUser.save().then(updatedUser => {
                    resp.json({ id: updatedUser.id })
                }).catch(error => {
                    resp.json({ error: error.message })
                })
            }
        }).catch(error => {
            resp.json({ error: error.message })
        })
    },
    async delete(req, resp) {
        const { id } = req.params
        User.findByPk(id).then(user => {
            user.destroy().then(() => {
                resp.status(200)
            }).catch(error => {
                resp.json({ error: error.message })
            })
        }).catch(error => {
            resp.json({ error: error.message })
        })
    }
}
