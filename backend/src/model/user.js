import sequelize from 'sequelize'

const user = {
    firstName: {
        type: sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: sequelize.STRING,
        allowNull: false
    },
    email: {
        type: sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: {
                msg: 'Por favor, forneça um email válido!'
            }
        }
    },
    phone: {
        type: sequelize.STRING,
        allowNull: false
    },
    accessType: {
        type: sequelize.INTEGER,
        defaultValue: 2,
        validate: {
            isIn: {
                args: [[1, 2, '1', '2']],
                msg: 'Valor inválido para o acesso'
            }
        }
    },
    password: {
        type: sequelize.STRING,
        allowNull: false
    }
}

export default user
