import sequelize from 'sequelize'

const bookRent = {
    userId: {
        type: sequelize.INTEGER,
        allowNull: false,
    },
    bookId: {
        type: sequelize.INTEGER,
        allowNull: false,
    },
    startRent: {
        type: sequelize.DATE,
        allowNull: false
    },
    endRent: {
        type: sequelize.DATE,
        allowNull: false,
        validate: {
            isGreaterThan(value) {
                if (value.getTime() <= this.startRent.getTime()) {
                    throw new Error('Data de entrega do livro deve ser maior que a data de retirada')
                }
            }
        }
    },
    status: {
        type: sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
        validate: {
            isIn: {
                args: [[0, 1, '0', '1']],
                msg: 'Status informado é inválido'
            }
        }
    },
}

export default bookRent
