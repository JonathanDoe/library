import sequelize from 'sequelize'

const bookReview = {
    userId: {
        type: sequelize.INTEGER,
        allowNull: false,
    },
    bookId: {
        type: sequelize.INTEGER,
        allowNull: false,
    },
    commentary: {
        type: sequelize.STRING,
        allowNull: false
    },
    rateValue: {
        type: sequelize.INTEGER,
        allowNull: false,
        validate: {
            isIn: {
                args: [[1, 2, 3, 4, 5]],
                msg: 'Informe um valor válido para avaliação'
            }
        }
    },
}

export default bookReview
