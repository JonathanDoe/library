import sequelize from 'sequelize'

const book = {
    title: {
        type: sequelize.STRING,
        allowNull: false
    },
    resume: {
        type: sequelize.STRING,
        allowNull: false
    },
    author: {
        type: sequelize.STRING,
        allowNull: false
    },
    dateLaunch: {
        type: sequelize.DATE,
        allowNull: false,
        defaultValue: new Date().toUTCString()
    }
}

export default book
