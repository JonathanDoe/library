import conn from '../../config/conn.js'
import Book from './book.js'
import User from './user.js'
import BookRent from './bookRent.js'
import BookReview from './bookReview.js'

const user = conn.define('users', User)
const book = conn.define('books', Book)
const bookReview = conn.define('reviews', BookReview)
const bookRent = conn.define('rents', BookRent)

user.hasOne(bookRent)
user.hasOne(bookReview)

book.hasOne(bookRent)
book.hasMany(bookReview)

bookRent.belongsTo(user)
bookReview.belongsTo(user)
bookRent.belongsTo(book)
bookReview.belongsTo(book)

export default {
    user,
    book,
    bookReview,
    bookRent,
}

export {
    user,
    book,
    bookReview,
    bookRent,
}
