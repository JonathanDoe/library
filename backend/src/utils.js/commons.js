function replaceValues(oldObj, newObj) {
    console.log(oldObj.get('commentary'))

    Object.keys(newObj).forEach(key => {
        if (oldObj.rawAttributes.hasOwnProperty(key) && newObj[key] != oldObj.get(key)) {
            oldObj[key] = newObj[key]
        }
    })
}

export default {
    replaceValues
}
