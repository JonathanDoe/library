import { Router } from "express"
import BookController from "./controller/bookController.js"
import UserController from "./controller/userController.js"
import RentController from "./controller/rentController.js"
import ReviewController from "./controller/reviewController.js"

const routes = Router()

routes.get('/books', BookController.get)
routes.get('/book/:id', BookController.get)
routes.post('/books', BookController.add)
routes.put('/books', BookController.update)
routes.delete('/books/:id', BookController.delete)

routes.post('/book/:bookId/rent', RentController.rent)
routes.post('/book/:bookId/return', RentController.return)

routes.get('/book/:bookId/review', ReviewController.getReviews)
routes.post('/book/:bookId/review', ReviewController.comment)
routes.put('/book/:bookId/review', ReviewController.edit)

routes.get('/user/:id', UserController.get)
routes.post('/users', UserController.add)
routes.put('/users', UserController.update)
routes.delete('/users/:id', UserController.delete)


export default routes
