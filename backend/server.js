import http from "http"
import app from "./src/app.js"
import dotenv from "dotenv"
import db from "./config/db.js"

dotenv.config()

db().then(() => {
    const server = http.createServer(app)
    const port = process.env.PORT || process.env.NODE_PORT || 3001
    server.listen(port, console.log(`Server running at PORT: ${port}`))
}).catch(error => console.log(`Error: ${error}`))
