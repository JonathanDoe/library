import sequelize from 'sequelize'
import config from './config.js'

const seqOptions = {
    host: config.host,
    port: config.port,
    dialect: config.dialect
}

if (process.env.NODE_ENV === 'prod') {
    seqOptions.native = true
    seqOptions.ssl = true
}
const seq = new sequelize(config.base, config.user, config.pass, {
    host: config.host,
    port: config.port,
    dialect: config.dialect
})

export default seq



