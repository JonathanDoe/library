import bcrypt from 'bcrypt'
import { book, user, bookRent, bookReview } from '../src/model/models.js'

export default {
    async includes() {
        for (let i = 1; i <= 10; i++) {
            const title = `The Book ${i}`
            const resume = `The Book ${i} in the library `
            const author = 'Jhon Doe'

            await book.findOrCreate({
                where: {
                    title,
                    resume,
                    author,
                },
                defaults: {
                    title,
                    resume,
                    author,
                }
            })
        }

        await user.findOrCreate({
            where: {
                firstName: 'Jhon',
                lastName: 'Doe',
                email: 'jhon@doe.com',
            },
            defaults: {
                firstName: 'Jhon',
                lastName: 'Doe',
                email: 'jhon@doe.com',
                phone: '44994577844',
                accessType: '2',
                password: '12345678'
            }
        })

        await user.findOrCreate({
            where: {
                firstName: 'Kah',
                lastName: 'Doe',
                email: 'kah@doe.com',
            },
            defaults: {
                firstName: 'Kah',
                lastName: 'Doe',
                email: 'kah@doe.com',
                phone: '44998787858',
                accessType: '2',
                password: '12345678'
            }
        })

        await bookRent.findOrCreate({
            where: {
                bookId: 1,
                userId: 2
            },
            defaults: {
                bookId: 1,
                userId: 3,
                startRent: new Date(2021, 4, 1),
                endRent: new Date(2021, 4, 17),
                status: 0
            }
        })

        await bookRent.findOrCreate({
            where: {
                bookId: 2,
                userId: 2
            },
            defaults: {
                bookId: 2,
                userId: 3,
                startRent: new Date(2021, 4, 1),
                endRent: new Date(2021, 4, 17),
                status: 1
            }
        })

        await bookRent.findOrCreate({
            where: {
                bookId: 3,
                userId: 2,
            },
            defaults: {
                bookId: 3,
                userId: 2,
                startRent: new Date(2021, 4, 7),
                endRent: new Date(2021, 4, 20),
                status: 1
            }
        })

        await bookRent.findOrCreate({
            where: {
                bookId: 1,
                userId: 3,
            },
            defaults: {
                bookId: 4,
                userId: 3,
                startRent: new Date(2021, 4, 7),
                endRent: new Date(2021, 4, 20),
                status: 1
            }
        })

        await bookRent.findOrCreate({
            where: {
                bookId: 4,
                userId: 3,
            },
            defaults: {
                bookId: 4,
                userId: 3,
                startRent: new Date(2021, 4, 7),
                endRent: new Date(2021, 4, 20),
                status: 1
            }
        })

        await bookReview.findOrCreate({
            where: {
                bookId: 1,
                userId: 2,
            },
            defaults: {
                bookId: 1,
                userId: 2,
                commentary: 'Comentario Fraco',
                rateValue: 2,
                status: 1
            }
        })

        await bookReview.findOrCreate({
            where: {
                bookId: 2,
                userId: 2,
            },
            defaults: {
                bookId: 1,
                userId: 2,
                commentary: 'Comentario Legal',
                rateValue: 3,
                status: 1
            }
        })

        await bookReview.findOrCreate({
            where: {
                bookId: 2,
                userId: 2,
            },
            defaults: {
                bookId: 1,
                userId: 2,
                commentary: 'Comentario ótimo!',
                rateValue: 5,
                status: 1
            }
        })

        await bookReview.findOrCreate({
            where: {
                bookId: 3,
                userId: 2,
            },
            defaults: {
                bookId: 1,
                userId: 2,
                commentary: 'Comentario ótimo!',
                rateValue: 5,
                status: 1
            }
        })

        await bookReview.findOrCreate({
            where: {
                bookId: 1,
                userId: 3,
            },
            defaults: {
                bookId: 1,
                userId: 3,
                commentary: 'Comentario Incrível!',
                rateValue: 5,
                status: 1
            }
        })

        await bookReview.findOrCreate({
            where: {
                bookId: 4,
                userId: 3,
            },
            defaults: {
                bookId: 4,
                userId: 3,
                commentary: 'Comentario nice!',
                rateValue: 5,
                status: 1
            }
        })
    },
    async createDefaults() {
        const hash = bcrypt.hashSync('Admin', 10)
        await user.findOrCreate({
            where: {
                firstName: 'User',
                lastName: 'Admin'
            },
            defaults: {
                firstName: 'User',
                lastName: 'Admin',
                email: 'admin@example.com',
                phone: '5544999999999',
                accessType: 1,
                password: hash
            }
        })
    }
}
