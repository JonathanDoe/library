import fs from "fs"
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

function getJsonFile() {
    const json = fs.readFileSync(__dirname + '/config.json', { encoding: 'utf8' })
    if (!json) {
        console.log('Falha ao ler o arquivo')
    }
    try {
        return JSON.parse(json)
    } catch (error) {
        console.log('Falha ao converter o json para um objeto: ', error)
        return false
    }

}

const configObj = getJsonFile()

const env = process.env.NODE_ENV || "development"

export default configObj[env]
