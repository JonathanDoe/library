import coon from './conn.js'
import seed from './seeds.js'

async function init() {
    try {
        await coon.sync({ alter: false, force: false })
        await seed.createDefaults()
        await seed.includes()
    } catch (err) {
        console.log(err)
    }
}

export default init
